"use client";
import React, { useState, useEffect } from "react";
import { initializeConnector } from "@web3-react/core";
import { MetaMask } from "@web3-react/metamask";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Chip from "@mui/material/Chip";
import Stack from "@mui/material/Stack";
import {
  Container,
  Card,
  CardContent,
  TextField,
  Divider,
} from "@mui/material";
// Knowledge about Ether.js https://docs.ethers.org/v6/getting-started/
import { ethers } from "ethers";
import { formatEther, parseUnits } from "ethers";
import abi from "./abi.json";

const [metaMask, hooks] = initializeConnector(
  (actions) =>
    new MetaMask({
      actions,
    })
);
const { useChainId, useAccounts, useIsActivating, useIsActive, useProvider } =
  hooks;
const contractChain = 11155111;
const contractAddress = "0x804C78A97705c862769A626af2eBe7E405cDCb8d";

export default function Page() {
  const chainId = useChainId();
  const accounts = useAccounts();
  const isActive = useIsActive();
  const provider = useProvider();
  const [error, setError] = useState(undefined);
  const [balance, setBalance] = useState("");

  const [walletBalance, setWalletBalance] = useState(0);
  useEffect(() => {
    const fetchBalance = async () => {
      try {
        const signer = provider.getSigner();
        const smartContract = new ethers.Contract(contractAddress, abi, signer);
        const myBalance = await smartContract.balanceOf(accounts[0]);
        console.log(formatEther(myBalance));
        setBalance(formatEther(myBalance));
        setWalletBalance(formatEther(myBalance));
      } catch (error) {
        console.error("Error fetching balance:", error);
      }
    };

    if (isActive) {
      fetchBalance();
    }
  }, [isActive]);
  const [fValue, setFValue] = useState(0);

  const handleSetFValue = (event) => {
    setFValue(event.target.value);
  };

  const handleBuy = async () => {
    try {
      if (fValue <= 0) {
        return;
      }

      const signer = provider.getSigner();
      const smartContract = new ethers.Contract(contractAddress, abi, signer);
      const buyValue = parseUnits(fValue.toString(), "ether");
      const tx = await smartContract.buy({
        value: buyValue.toString(),
      });
      console.log("Transaction hash:", tx.hash);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    void metaMask.connectEagerly().catch(() => {
      console.debug("Failed to connect  to metamask");
    });
  }, []);

  const handleConnect = () => {
    metaMask.activate(contractChain);
  };

  const handleDisconnect = () => {
    metaMask.resetState();
  };

  function ResponsiveAppBar() {
    const [anchorElNav, setAnchorElNav] = useState(null);
    const [anchorElUser, setAnchorElUser] = useState(null);

    const handleOpenNavMenu = (event) => {
      setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
      setAnchorElUser(event.currentTarget);
    };
  }

  const [showSendToken, setShowSendToken] = useState(false);

  const handleOpenSendToken = () => {
    setShowSendToken(true);
  };

  const handleCloseSendToken = () => {
    setShowSendToken(false);
  };

  const [toAddress, setToAddress] = useState("");
  const [amount, setAmount] = useState("");

  const transferToAccount = async () => {
    try {
      if (!toAddress || !amount) {
        console.error("Please enter both address and amount");
        return;
      }

      const signer = provider.getSigner();
      const smartContract = new ethers.Contract(contractAddress, abi, signer);

      const result = await smartContract.transfer(toAddress, amount);
      console.log("Transaction result:", result);
    } catch (error) {
      console.error("Error transferring funds:", error);
    }
  };

  return (
    <div>
      <Box
        Box
        sx={{
          flexGrow: 1,
        }}
      >
        <AppBar position="static" sx={{ backgroundColor: "black" }}>
          <Toolbar>
            <img
              src="https://e7.pngegg.com/pngimages/377/519/png-clipart-f-f.png  " // URL ของรูปภาพโลโก้
              alt="Logo"
              style={{
                height: 48,
                width: "auto",
                marginRight: "10px",
              }} // ปรับขนาดตามความต้องการ
            />
            <Typography
              Typography
              variant="h5"
              component="div"
              sx={{
                flexGrow: 1,
              }}
            >
              Token Crypto
            </Typography>

            {isActive ? (
              <Stack direction="row" spacing={1}>
                <Button
                  color="inherit"
                  onClick={handleOpenSendToken}
                  sx={{
                    fontSize: "1rem",
                    color: "green",
                  }}
                >
                  SendToken
                </Button>
                <Button onClick={handleCloseSendToken}>Cancel</Button>
                <Button
                  Button
                  color="inherit"
                  onClick={handleDisconnect}
                  sx={{
                    fontSize: "1rem",
                    color: "red",
                  }}
                >
                  Disconnect
                </Button>
              </Stack>
            ) : (
              <Button
                Button
                color="inherit"
                onClick={handleConnect}
                sx={{
                  fontSize: "1rem",
                  color: "cyan",
                }}
              >
                Connect Your Wallet
              </Button>
            )}
          </Toolbar>
        </AppBar>

        {isActive && (
          <Container maxWidth="sm">
            <Box id="box1">
              <Card
                id="card1"
                Card
                sx={{
                  padding: 2,

                  maxWidth: 450,

                  borderStyle: "solid",
                }}
              >
                <CardContent>
                  <img
                    id="imglo"
                    src="https://cdn-icons-png.flaticon.com/512/272/272525.png" // URL ของรูปภาพโลโก้
                    alt="Logo"
                    style={{
                      height: 48,
                      width: "auto",
                    }} // ปรับขนาดตามความต้องการ
                  />
                  <Typography id="h6" variant="h4">
                    Your wallet balance
                  </Typography>
                  <TextField
                    className="tf2"
                    id="outlined-read-only-input"
                    label="Address"
                    defaultValue=" "
                    value={accounts}
                    InputProps={{
                      readOnly: true,
                    }}
                  />

                  <TextField
                    className="tf2"
                    id="outlined-read-only-input"
                    label="FToken Balance"
                    defaultValue=" "
                    value={balance}
                    InputProps={{
                      readOnly: true,
                    }}
                  />

                  <Typography id="tt1" variant="body1">
                    Buy F Token
                  </Typography>
                  <TextField
                    className="tf2"
                    required
                    id="outlined-required"
                    label="Enter amount of Ether you want to buy F Token"
                    defaultValue=""
                    type="number"
                    onChange={handleSetFValue}
                  />
                </CardContent>
                <Button id="btn" variant="contained" onClick={handleBuy}>
                  Buy F Token
                </Button>
              </Card>
            </Box>
          </Container>
        )}

        {isActive && showSendToken && (
          <Container maxWidth="sm" sx={{ mt: 2 }}>
            <Card
              id="card2"
              Card
              sx={{
                padding: 2,

                maxWidth: 450,

                borderStyle: "solid",
              }}
            >
              <CardContent>
                <img
                  id="imglo2"
                  src="https://cdn.iconscout.com/icon/free/png-256/free-send-bitcoin-1824277-1545905.png " // URL ของรูปภาพโลโก้
                  alt="Logo"
                  style={{
                    height: 48,
                    width: "auto",
                  }} // ปรับขนาดตามความต้องการ
                />

                <Stack spacing={2}>
                  <Typography id="h6" variant="h6">
                    Send your token to another accounts.
                  </Typography>

                  <TextField
                    className="tf2"
                    input
                    type="text"
                    label="To [Example 0x573......]"
                    value={toAddress}
                    variant="outlined"
                    onChange={(e) => setToAddress(e.target.value)}
                  />

                  <TextField
                    required
                    id="outlined-required"
                    label="Enter amount of Ether you want to send for someone."
                    defaultValue={amount}
                    type="number"
                    onChange={(e) => setAmount(e.target.value)}
                  />

                  <Button
                    id="btn2"
                    variant="contained"
                    onClick={transferToAccount}
                  >
                    Send
                  </Button>
                </Stack>
              </CardContent>
            </Card>
          </Container>
        )}
      </Box>

      <br></br>
    </div>
  );
}
